﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace WorkspaceAdjuster
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> _workspacePaths = new List<string>();
        private string _npString = "NP";
        private string _sctString = "SCT";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Select_Workspaces(object sender, RoutedEventArgs e)
        {
            var statusLabel = StatusLabel;
            string initialDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "NinjaTrader 8", "workspaces");

            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = true;
            // set initial Directory to nt workspace directory if possible
            dlg.InitialDirectory = Directory.Exists(initialDirectory) ? initialDirectory : Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".xml";
            //dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";


            // Display OpenFileDialog by calling ShowDialog method 
            bool? result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                StatusLabel.Content = $"Selected {dlg.FileNames.Count()} workspaces";
                _workspacePaths.AddRange(dlg.FileNames);
            }
        }

        private void Adjust_Workspaces(object sender, RoutedEventArgs e)
        {
            //check direction of adjustment =======================
            if (NPtoSCT.IsChecked == null)
            {
                StatusLabel.Content = "error in radiobutton group";
                return;
            }

            string pattern, replacement;

            if ((bool)NPtoSCT.IsChecked)
            {
                pattern = _npString;
                replacement = _sctString;
                StatusLabel.Content = CreateStatusString(true);
            }
            else
            {
                pattern = _sctString;
                replacement = _npString;
                StatusLabel.Content = CreateStatusString(false);
            }
            // ====================================================

            //adjust workspaces
            foreach (var path in _workspacePaths)
            {
                AdjustWorkspace(path, pattern, replacement);
            }
            _workspacePaths = new List<string>();

            StatusLabel.Content = "Done!";
        }

        private object CreateStatusString(bool direction)
        {
            return $"Selected {_workspacePaths.Count()} workspaces; Converting from {(direction ? "NP to SCT" : "SCT to NP")}";
        }

        private void AdjustWorkspace(string path, string pattern, string replacement)
        {
            var workspaceString = File.ReadAllText(path);

            workspaceString = Regex.Replace(workspaceString, pattern, replacement, RegexOptions.Multiline);

            File.Delete(path);
            File.WriteAllText(path, workspaceString);
        }
    }
}
